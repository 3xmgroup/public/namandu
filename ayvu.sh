#!/bin/bash

# This script try to be a setup script for Ñamandú for AWS
export PROJECTID=19195241
export PROJECT_FILE=projects.yaml
export ACCESSTOKEN

# TODO
# - Opinionated values
# ̣- Environment variable per branch
# - K8S files for environments

function configure_aws
{
  declare -A credentials 
  read -p "We will setup AWS variables, do you want to continue? (yes/no) [yes]: " QUESTION
  QUESTION=${QUESTION:-yes}
  if [ $QUESTION = "yes" ]
  then
    echo "Please enter the AWS Access Key ID: "
    read -s credentials[AWS_ACCESS_KEY_ID]
    echo "Please enter the AWS Secret Access Key: " 
    read -s credentials[AWS_SECRET_ACCESS_KEY]
    echo "Please enter the AWS Default Region: "
    read credentials[AWS_DEFAULT_REGION]
    for KEY in "${!credentials[@]}"; do
      load_variables
    done
  fi
}

function configure_token 
{
  # if no provided will be loaded with GITLAB_ACCESS_TOKEN Variable, if any
  read -p "Please provide your github Access Token: " ACCESSTOKEN
  ACCESSTOKEN=${ACCESSTOKEN:-$GITLAB_ACCESS_TOKEN}
}

function load_variables
{
  environment=`git rev-parse --abbrev-ref HEAD`
  if [[ $KEY == *"PASS" || $KEY == "AWS"*"ACCESS"* ]]; then
    MASK="true"
  else
    MASK="false"
  fi 
  if [[ $environment == "master" ]]; then
    environment="*"
  fi
  if [[ $KEY == "KUBECONFIG" ]]; then
    TYPE="file"
  else
    TYPE="env_var"
  fi
  curl -s --request POST --header "PRIVATE-TOKEN: ${ACCESSTOKEN}" "https://gitlab.com/api/v4/projects/${PROJECTID}/variables" --form "key=${KEY}" --form "value=${credentials[$KEY]}" --form "masked=${MASK}" --form "environment_scope=${environment}" --form "variable_type=${TYPE}"
}

function configure_variables 
{
  a=0
  t=`yq -r .[].name $PROJECT_FILE | wc -l`
  declare -A credentials

  while [ $a -lt $t ]
  do
    name=`yq -r .[$a].name $PROJECT_FILE`
    url=`yq -r .[$a].url $PROJECT_FILE`
    devbranch=`yq -r .[$a].devbranch $PROJECT_FILE`

      # Load values from PROJECT_FILE
      urlname=${name//-/}
      urluser=${urlname^^}USERNAME
      urlpass=${urlname^^}PASS
      echo
      read -p "We will setup the environment variables for ${name}, do you want to continue? (yes/no) [yes]: " QUESTION
      QUESTION=${QUESTION:-yes}
      if [ $QUESTION = "yes" ]
      then
        echo "...setting up variable for ${name}..."
        echo "Provide the username for ${urluser} variable: "
        read credentials[${urluser}]
        echo "now provide the password for ${urlpass} variable: "
        read -s credentials[${urlpass}]
      fi
      a=$(( $a + 1 ))
    done

  # Will loop the associative array with credentials and load Ñamandú variables on Gitlab...
  for KEY in "${!credentials[@]}"; do
    load_variables
  done
}

function configure_cluster
{
  declare -A credentials
  environment=`git rev-parse --abbrev-ref HEAD`
  read -p "We will setup a Kubernetes cluster for the environment ${environment}, do you want to continue? (yes/no) [yes]: " QUESTION
  QUESTION=${QUESTION:-yes}
  if [ $QUESTION = "yes" ]
  then
    read -p "Provide the path to the kubeconfig file (content of KUBECONFIG would be loaded if empty): " kubepath 
    kubepath=${kubepath:-$KUBECONFIG}
    credentials[KUBECONFIG]=`cat $kubepath`
  fi
  for KEY in "${!credentials[@]}"; do
    load_variables
  done
}

function configure_helm
{
  declare -A credentials
  environment=`git rev-parse --abbrev-ref HEAD`
  read -p "We will setup a git repository with helm charts for the environment ${environment}, do you want to continue? (yes/no) [yes]: " QUESTION
  QUESTION=${QUESTION:-yes}
  if [ $QUESTION = "yes" ]
  then
    read -p  "Provide the url in https format for the repository (for private repository please include de username and password into the url): " helmrepo
    credentials[HELMCHARTS]=$helmrepo
  fi
  for KEY in "${!credentials[@]}"; do
    load_variables
  done
}

configure_token
configure_aws
configure_variables
configure_cluster
configure_helm
